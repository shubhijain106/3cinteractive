
import java.io.*;
import java.util.*;

public class Listview 
{
    
            public static void main(String[] args)
    {
        try
        {
            InputStreamReader in=new InputStreamReader(System.in);
            BufferedReader in1=new BufferedReader(in);
            ArrayList arr=new ArrayList<String>();       //list to store tasks
            while(true)
            {
                
                System.out.print("Input:");
                String input=in1.readLine();
                String[] parts=input.split("> ");            //parts[0] stores task to be performed
                                                             //parts[1] stores corresponding work
                //System.out.println(parts[0]);
                if(parts[0].equals(" <add") || parts[0].equals("<add")  )   //robust code for input with and without initial space
                {
                    arr.add(parts[1]);
                    System.out.println("Output: Added with id#"+(arr.indexOf(parts[1])+1));
                }
                else if(parts[0].equals(" <list>") || parts[0].equals("<list>") )
                {
                  if(arr.size()==0)
                      System.out.println("Output: List is empty. Add a task");
                  else
                  {
                      System.out.println("Output:");
                      for(int i=0;i<arr.size();i++)
                        {
                            System.out.println((i+1)+". "+arr.get(i));        //display all the items of the list
                        }
                  }
                }
                else if(parts[0].equals(" <done")|| parts[0].equals("<done"))
                {
                    
                    if(arr.size()==0)
                      System.out.println("Output: List is empty. No more task to perform.");  
                    
                    else if(arr.size()>=Integer.parseInt(parts[1]))
                    {
                        arr.remove(Integer.parseInt(parts[1])-1);           //removes the task performed
                        System.out.println("Output:"); 
                        for(int i=0;i<arr.size();i++)
                        {
                            System.out.println((i+1)+". "+arr.get(i));
                        }
                    }
                    else
                    {
                        System.out.println("Output: Incorrect input");
                    }
                    
                }
                else if(parts[0].equals(" <exit>")|| parts[0].equals("<exit>"))
                {
                    System.out.println("Output: exit ");
                    break;
                }
                else
                {
                  System.out.println("Output: Incorrect input ");  
                }
                
            }
        }
        catch(IOException e){}
    }
}

